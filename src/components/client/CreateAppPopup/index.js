import React, {useState} from "react";
import './style.css'
import Popup from "../../common/Popup";
import Button from "../../common/kit/Button";
import {createApplicationRequest} from "./service";

export default function CreateAppPopup(props) {

    let hide = props.hide
    let [type, setType] = useState('OTHER')
    let [description, setDescription] = useState('')

    const close = () => {
        setType('OTHER')
        setDescription('')
        props.close()
    }

    return (
        hide !== true ?
            <Popup>
                <div className="create-app-popup">
                    <h1>Создание заявки</h1>

                    <select onChange={e => setType(e.target.value)}>
                        <option value="NOT_WORK_INTERNET">Не работает интернет</option>
                        <option value="CHANGE_TARIFF">Изменение тарифа</option>
                        <option value="QUESTION">Вопрос</option>
                        <option value="OTHER">Другое</option>
                    </select>

                    <textarea placeholder="Описание" value={description}
                              onChange={e => setDescription(e.target.value)}/>

                    <Button action text="Готово" onClick={() => createApplication(type, description, close)}/>
                </div>
            </Popup>
            :
            <></>
    )
}

const createApplication = (type, description, close) => {
    createApplicationRequest(type, description)
    close()
    setTimeout(() => window.location.reload(), 1000)
}
