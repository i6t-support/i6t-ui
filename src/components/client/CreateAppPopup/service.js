import {USER_URL, post} from "../../utils/api";

export const createApplicationRequest = (reason, appealText) => {
    const body = {reason, appealText}
    post(`${USER_URL}/api/v1/user/app`, body)
}