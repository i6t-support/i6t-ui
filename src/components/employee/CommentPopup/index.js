import React, {useState} from "react";
import './style.css'
import Popup from "../../common/Popup";
import Button from "../../common/kit/Button";

export default function CommentPopup(props) {

    let [comment, setComment] = useState('')

    const {onClick, state} = props
    return (
        state !== 'HIDDEN' ?
            <Popup>
                <div className="comment-popup">
                    <h1>Оставьте комментарий</h1>

                    <textarea placeholder="Комментарий" value={comment} onChange={e => setComment(e.target.value)}/>

                    <Button action text="Готово" onClick={() => onClick(state, comment)}/>
                </div>
            </Popup>
            :
            <></>
    )
}