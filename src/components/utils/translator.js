export const resolveReason = (reason) => {
    if (reason === 'CHANGE_TARIFF') {
        return 'Изменение тарифа'
    } else if (reason === 'NOT_WORK_INTERNET') {
        return 'Не работает интернет'
    } else if (reason === 'QUESTION') {
        return 'Вопрос'
    } else if (reason === 'OTHER') {
        return 'Другой вопрос'
    } else {
        return reason;
    }
}

export const resolveDate = (date) => {
    if (!date)
        return undefined;

    const moment = new Date(date)
    return `${dateValueToStr(moment.getDate())}.${dateValueToStr(moment.getMonth() + 1)}.${moment.getFullYear()}`
}

export const resolveTime = (time) => {
    if (!time)
        return undefined;

    const moment = new Date(time)
    return `${resolveDate(time)} ${dateValueToStr(moment.getHours())}:${dateValueToStr(moment.getMinutes())}`
}

export const fio = (f, i, o) => {
    return f ? (`
    ${f}
    ${i && i.substr(0, 1) + '.'}
    ${o && o.substr(0, 1) + '.'}
    `) : undefined
}

export const resolveAppStatus = (status) => {
    const convert = []
    convert['CREATED'] = 'Создано'
    convert['DELEGATE_TO_OPERATOR'] = 'Назначено на оператора'
    convert['DELEGATE_TO_OPERATOR_FOR_CLOSE'] = 'Назначено на оператора'
    convert['DELEGATE_TO_SUPPORT'] = 'Назначено на поддержку'
    convert['DELEGATE_TO_TECH_SPEC'] = 'Назначено на тех. специалиста'
    convert['OPERATOR_IN_WORK'] = 'В работе у оператора'
    convert['OPERATOR_SEARCH'] = 'Поиск оператора'
    convert['OPERATOR_WAITING'] = 'В ожидании у оператора'
    convert['SUPPORT_IN_WORK'] = 'В работе у поддержки'
    convert['SUPPORT_WAITING'] = 'В ожидании у поддержки'
    convert['TECH_SPEC_IN_WORK'] = 'В работе у тех. спец.'
    convert['TECH_SPEC_WAITING'] = 'В ожидании у тех. спец.'
    convert['WAITING_APPROVE_CLIENT'] = 'Завершено'
    convert['CLIENT_APPROVE_AND_CLOSED'] = 'Завершено'
    return convert[status]
}

const dateValueToStr = (value) => {
    return value < 10 ? '0' + value : value
}