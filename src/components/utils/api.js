import axios from 'axios'

const USER_URL = process.env.REACT_APP_API_USER_URL
const APP_URL = process.env.REACT_APP_API_APP_URL
const NOTIFICATION_URL = process.env.REACT_APP_API_NOTIFICATION_URL

function getAuthHeaders() {
    const token = localStorage.getItem('token')

    if (token) {
        return {
            'Authorization': token,
        }
    }
}

export function post(endpoint, data, headers = {}) {
    const config = {
        headers: {
            ...headers,
            ...getAuthHeaders()
        }
    }
    return axios.post(endpoint, data, config)
}

export function del(endpoint, headers = {}) {
    const config = {
        headers: {
            ...headers,
            ...getAuthHeaders()
        }
    }
    return axios.delete(endpoint, config)
}

export function upload(endpoint, data, headers = {}) {
    const config = {
        headers: {
            ...headers,
            ...getAuthHeaders(),
            'Content-Type': 'multipart/form-data'
        }
    }
    return axios.post(endpoint, data, config)
}

export function patch(endpoint, data, headers = {}) {
    const config = {
        headers: {
            ...headers,
            ...getAuthHeaders()
        }
    }
    return axios.patch(endpoint, data, config)
}

export function get(endpoint, headers = {}) {
    const config = {
        headers: {
            ...headers,
            ...getAuthHeaders()
        }
    }
    return axios.get(endpoint, config)
}

export {USER_URL, APP_URL, NOTIFICATION_URL}