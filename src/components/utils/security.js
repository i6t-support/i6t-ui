import {USER_URL, get} from "./api";

export default async function currentUser() {
    return await get(USER_URL + '/api/v1/user/current')
        .then(r => r.data)
}

export function userRole() {
    const bearerToken = localStorage.getItem('token')
    if (!bearerToken)
        return undefined

    const token = bearerToken.replace('Bearer ', '')
    const body = token.split('.')[1]
    const decode = window.atob(body)
    const json = JSON.parse(decode)

    return json.authority.name
}

export function isUser() {
    return userRole() === 'ROLE_USER'
}

export function isStaff() {
    return ['ROLE_OPERATOR', 'ROLE_TECH_SPEC', 'ROLE_SUPPORT', 'ROLE_ADMIN'].includes(userRole())
}