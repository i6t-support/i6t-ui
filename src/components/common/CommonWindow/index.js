import './style.css'
import React, {useState} from "react";
import Header from "../kit/Header";
import Button from "../kit/Button";
import AboutTag from "../AboutTag";
import WorkBoard from "../WorkBoard";
import {useHistory} from "react-router-dom";
import CreateAppPopup from "../../client/CreateAppPopup";
import {isUser} from "../../utils/security";

export default function CommonWindow() {
    const history = useHistory()

    if (!isAuthorized()) {
        history.push('/login')
    }

    let [isCreateApp, setCreateApp] = useState(false)

    return (
        <div className="common-window">
            <CreateAppPopup hide={!isCreateApp} close={() => setCreateApp(false)}/>

            <Header>
                <Button text="Главная"/>
                {isUser() && <Button text="Создать заявку" onClick={() => setCreateApp(true)}/>}
            </Header>

            <AboutTag/>

            <WorkBoard/>
        </div>
    )
}

const isAuthorized = () => {
    return localStorage.getItem('token') && true
}
