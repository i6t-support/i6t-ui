import React, {useEffect, useState} from "react";
import './style.css'
import SideBarElement from "../SideBarElement";
import {APP_URL, get} from "../../utils/api";
import {Link, useParams} from "react-router-dom";

export default function SideBar(props) {
    let [list, setList] = useState([])
    let {applicationId} = useParams()

    useEffect(
        () => loadApplications().then(r => setList(r.content)),
        []
    )

    const {openApplication} = props
    return (
        <>
            <div className="sidebar">
                {
                    list.map(application => {
                        return (
                            <Link key={application.id} to={`/application/${application.id}`}>
                                <SideBarElement
                                    component={Link}
                                    to
                                    key={application.id}
                                    application={application}
                                    onClick={openApplication}
                                    active={applicationId && application.id === applicationId}
                                />
                            </Link>

                        )
                    })
                }
            </div>
        </>


    )
}

const loadApplications = () => {
    return get(`${APP_URL}/api/v1/list`)
        .then(r => r.data)
}