import React from "react";
import './style.css'

export default function Popup(props) {
    return (
        <div className="popup">
            <div className="popup-content">
                {props.children}
            </div>
        </div>

    )
}