import React from "react";
import './style.css'
import {resolveAppStatus, resolveDate, resolveReason, resolveStatus} from "../../utils/translator";

export default function SideBarElement(props) {

    const isActive = props.active;
    const background = isActive ? '#FDDCA5' : 'none'
    const application = props.application
    const openApplication = props.onClick
    const setApplication = openApplication ? openApplication : console.log

    const {id, reason, createdAt, applicationStatus} = application

    return (
        <div className="sidebar-elem" style={{background}} onClick={() => setApplication(application)}>
            <div className="sidebar-elem-top-line">
                <h2>
                    {resolveReason(reason)}
                </h2>
                <h3>
                    #{id.split('-')[0].toUpperCase()}
                </h3>
            </div>
            <div className="sidebar-elem-bottom-line">
                <h2>
                    {resolveAppStatus(applicationStatus)}
                </h2>
                <h3>
                    от {resolveDate(createdAt)}
                </h3>
            </div>
        </div>
    )
}
