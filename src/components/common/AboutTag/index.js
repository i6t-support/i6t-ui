import React, {useEffect, useState} from "react";
import './style.css'
import currentUser, {isUser, userRole} from "../../utils/security";
import {fio} from "../../utils/translator";

export default function AboutTag() {
    let [user, setUser] = useState()

    const loadUser = () => currentUser().then(v => setUser(v))

    useEffect(
        loadUser,
        []
    )

    const {secondName, firstName, patronymic} = user || {}

    return (
        <div className="about-tag">
            <div className="about-tag-name">
                {user && fio(secondName, firstName, patronymic)}
            </div>
            <div className="about-tag-sub">
                {user && (
                    isUser()
                        ? 'Договор: 1231231234'
                        : role(userRole())
                )}
            </div>
        </div>
    )
}


const role = (raw) => {
    if (raw === 'ROLE_OPERATOR') {
        return 'Оператор'
    } else if (raw === 'ROLE_TECH_SPEC') {
        return 'Тех. специалист'
    } else if (raw === 'ROLE_SUPPORT') {
        return 'Служба поддержки'
    } else if (raw === 'ROLE_USER') {
        return 'Клиент'
    } else {
        return raw
    }
}