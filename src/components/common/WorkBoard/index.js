import React, {useState} from "react";
import './style.css'
import ApplicationBoard from "../ApplicationBoard";
import SideBar from "../SideBar";
import CommentPopup from "../../employee/CommentPopup";
import {useHistory, useParams} from "react-router-dom";
import {close, complete, delegate} from "./service";

export default function WorkBoard() {
    let {applicationId} = useParams()
    const history = useHistory()

    let [commentPopupState, setCommentPopupState] = useState('HIDDEN')
    let [application, setApplication] = useState()
    const openApplication = (application) => setApplication(application)

    const showCommentPopup = (state) => {
        setCommentPopupState(state)
    }

    const commentSubmit = (state, comment) => {
        if (applicationId) {
            history.push("/");
            if (
                [
                    'DELEGATE_TO_TECH_SPEC',
                    'DELEGATE_TO_OPERATOR',
                    'DELEGATE_TO_SUPPORT'
                ].includes(state)
            ) {
                delegate(state, applicationId, comment)
            } else if ('CLOSE' === state) {
                close(applicationId, comment)
            } else if ('COMPLETE' === state) {
                complete(applicationId, comment)
            }

            setTimeout(() => window.location.reload(), 1000)
        }

        setCommentPopupState('HIDDEN');
    }

    return (
        <div className="work-board">
            <CommentPopup state={commentPopupState} onShow={commentPopupState} onClick={commentSubmit}/>
            <SideBar openApplication={openApplication}/>
            <ApplicationBoard updateApplication={openApplication} showCommentPopup={showCommentPopup}/>
        </div>
    )
}
