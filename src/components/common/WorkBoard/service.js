import {APP_URL, post} from "../../utils/api";

export const delegate = (state, applicationId, comment) => {
    post(`${APP_URL}/api/v1/delegate?applicationId=${applicationId}&comment=${comment}&delegateStatus=${state}`)
}


export const complete = (applicationId, comment) => {
    post(`${APP_URL}/api/v1/complete?applicationId=${applicationId}&comment=${comment}`)
}


export const close = (applicationId, comment) => {
    post(`${APP_URL}/api/v1/close?applicationId=${applicationId}&comment=${comment}`)
}

