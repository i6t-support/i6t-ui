import './style.css'
import TextField from "../kit/TextField";
import SimpleButton from "../kit/SimpleButton";
import React, {useState} from "react";
import {authorize} from "./service";
import StatusCode from "../kit/StatusCode";
import { useHistory } from "react-router-dom";

export default function AuthWindow() {
    const history = useHistory()
    if (isAuthorized()) {
        history.push('/')
    }

    let [login, setLogin] = useState('')
    let [password, setPassword] = useState('')
    let [error, setError] = useState()

    return <div className="auth">
        <h1>Вход</h1>
        <TextField placeholder="Логин" onChange={e => setLogin(e.target.value)}/>
        <TextField password placeholder="Пароль" onChange={e => setPassword(e.target.value)}/>
        <div className="auth-left-bar">
            <SimpleButton text="Войти" onClick={() => authorize(login, password, setError)}/>
        </div>
        <StatusCode error={error}/>
    </div>
}

const isAuthorized = () => {
    return localStorage.getItem('token') && true
}
