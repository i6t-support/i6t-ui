import {USER_URL, post} from "../../utils/api";

export function authorize(login, password, showError) {
    post(USER_URL + '/security/auth', {login, password})
        .then(r => saveToken(r.data))
        .catch(e => showError(e.response.data))
}

const saveToken = token => {
    localStorage.setItem('token', 'Bearer ' + token)
    window.location.reload()
}