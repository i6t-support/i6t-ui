import React, {useEffect, useState} from "react";
import './style.css'
import Button from "../kit/Button";
import {fio, resolveReason, resolveTime} from "../../utils/translator";
import StatusCode from "../kit/StatusCode";
import {loadApplication, startWork} from "./service";
import {useParams} from "react-router-dom"
import {isStaff, userRole} from "../../utils/security";

export default function ApplicationBoard(props) {


    let {applicationId} = useParams()
    let [application, setApplication] = useState({})
    let [error, setError] = useState()

    const {updateApplication, showCommentPopup} = props

    useEffect(
        () => loadApplication(applicationId).then(r => setApplication(r)),
        [applicationId]
    )

    const {
        id,
        reason,
        createdAt,
        address,
        appealText,
        applicationStatus,
        contractNumber,
        firstNameClient,
        lastNameClient,
        patronymicClient,
        phoneNumber,
        updatedAt
    } = application || {}

    const role = userRole()

    return id ?
        (
            <div className="application-board">
                <div className="application-board-header">
                    <h1>{resolveReason(reason)}</h1>
                    <h2>{createdAt && `от ${resolveTime(createdAt)}`}</h2>
                </div>

                <div className="application-board-content">
                    <ApplicationParam title="Описание" content={appealText}/>
                    <ApplicationParam title="Адрес клиента" content={address}/>
                    <ApplicationParam title="Номер договора" content={`#${contractNumber || ''}`}/>
                    <ApplicationParam title="ФИО клиента"
                                      content={fio(lastNameClient, firstNameClient, patronymicClient)}/>
                    <ApplicationParam title="Номер телефона" content={phoneNumber}/>
                    <ApplicationParam title="Последнее изменение" content={resolveTime(updatedAt)}/>
                </div>

                {
                    application.comments && application.comments.length !== 0 &&
                    <div className="application-board-comments">
                        <h3>Комментарии: </h3>
                        {
                            application.comments.map(comment =>
                                <Comment content={comment}/>
                            )
                        }
                    </div>
                }

                {isStaff() && (
                    isNotStarted(applicationStatus) ? (
                        <div className="application-board-actions">
                            <Button action text="В работу"
                                    onClick={() => startWork(id, setError) || refresh(id, updateApplication)}/>
                        </div>
                    ) : (
                        <div className="application-board-actions">
                            <div className="dropdown">
                                <Button action text="Делегировать"/>
                                <div className="dropdown-content">
                                    {
                                        role !== 'ROLE_TECH_SPEC' &&
                                        <button onClick={() => showCommentPopup('DELEGATE_TO_TECH_SPEC')}>Тех.
                                            Специалист</button>
                                    }
                                    {
                                        role !== 'ROLE_SUPPORT' &&
                                        <button
                                            onClick={() => showCommentPopup('DELEGATE_TO_SUPPORT')}>Поддержка</button>
                                    }

                                </div>
                            </div>
                            <span/>
                            {
                                role !== 'ROLE_OPERATOR' &&
                                <Button action onClick={() => showCommentPopup('COMPLETE')}
                                        text="Завершить"/>
                            }
                            {
                                role === 'ROLE_OPERATOR' &&
                                <Button action onClick={() => showCommentPopup('CLOSE')} text="Закрыть"/>
                            }
                        </div>
                    )
                )}

                <StatusCode error={error}/>


            </div>
        ) : <></>
}

const Comment = (props) => {
    const {id, text, createdAt, author} = props.content
    const {firstName, secondNameInitial} = author

    return (
        <div key={id} className="application-board-comment">
            <div className="application-board-comment-title">
                <h4>{firstName} {secondNameInitial}.</h4>
                <span>{resolveTime(createdAt)}</span>
            </div>
            <div className="application-board-comment-text">
                {text}
            </div>
        </div>
    )
}

const refresh = (applicationId, updateApplication) => {
    updateApplication(loadApplication(applicationId));
}

const ApplicationParam = (props) => {
    const {title, content} = props
    return (
        <div className="application-board-param">
            <h3>{title}:</h3>
            <p>
                {content}
            </p>
        </div>
    )
}

const isNotStarted = (status) => {
    return ['OPERATOR_WAITING', 'SUPPORT_WAITING', 'TECH_SPEC_WAITING'].includes(status)
}
