import {APP_URL, get, post} from "../../utils/api";

export const startWork = (applicationId, showError) => {
    post(`${APP_URL}/api/v1/start?applicationId=${applicationId}`)
        .then(() => showError(undefined))
        .then(() => window.location.reload())
        .catch(e => showError(e.response.data))
}

export const loadApplication = async (applicationId) => {
    const content = await get(`${APP_URL}/api/v1/list`)
        .then(r => r.data.content)

    const filtered = content.filter(app => app.id === applicationId)
    return filtered.length === 1 ? filtered[0] : undefined
}