import React from "react";
import './style.css'

export default function TextField(props) {
    const {text, onChange, placeholder, password} = props
    const type = password ? 'password' : 'text'
    return (
        <div className="text-field">
            <input type={type} placeholder={placeholder} onChange={onChange}>
                {text}
            </input>
        </div>
    )
}