import React from "react";
import './style.css'

export default function SimpleButton(props) {
    const {text, onClick} = props
    return (
        <div className="simple-button">
            <button onClick={onClick}>
                {text}
            </button>
        </div>
    )
}