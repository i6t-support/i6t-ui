import React from "react";
import './style.css'

export default function StatusCode(props) {
    const {error} = props
    if (error) {
        const type = error.class
        const message = resolveMessage(type) || error.message
        return (
            <div className="status-code">
                {message}
            </div>
        )
    } else {
        return <></>
    }
}

const resolveMessage = (type) => {
    if (type === 'UserNotFoundException') {
        return 'Пользователь не найден'
    } else if (type === 'AuthorizeUserException') {
        return 'Ошибка авторизации'
    }
}