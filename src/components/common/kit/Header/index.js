import React from "react";
import './style.css'
import Button from "../Button";
import logout from "./service";

export default function Header(props) {
    return (
        <div className="header">
            {props.children}

            <div className="header-logout">
                <Button text="Выйти" onClick={() => logout()}/>
            </div>
        </div>

    )
}