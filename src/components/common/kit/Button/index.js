import React from "react";
import './style.css'

export default function Button(props) {
    const {text, onClick, action} = props
    const className = action ? "button-action" : "button"
    return (
        <div className={className}>
            <button onClick={onClick}>
                {text}
            </button>
        </div>

    )
}