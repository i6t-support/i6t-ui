import './App.css';
import AuthWindow from './components/common/AuthWindow'
import React from "react";
import CommonWindow from "./components/common/CommonWindow";
import {BrowserRouter, Route, Switch} from "react-router-dom";

function App() {

    return (
        <div className="App">
            <BrowserRouter basename="/">
                <Switch>
                    <Route exact path="/">
                        <CommonWindow/>
                    </Route>
                    <Route path="/login">
                        <AuthWindow/>
                    </Route>
                    <Route path="/application/:applicationId">
                        <CommonWindow/>
                    </Route>
                </Switch>
            </BrowserRouter>

        </div>
    );
}

export default App;


